#init de instalación de programas desde el servidor de clase, conexión con servidor y BackUp
#Autor: Fernando Johanson
#Versión:3.0
#contenido: nodejs, npm, sublimetext3

#variables:
PENDRIVE="Loki"
pendrive_path="/media/user/${PENDRIVE}"
Servidor="192.168.100.76"
Usuario="johan"
Destino="${Usuario}@${Servidor}"
KG="ssh-keygen"
KC="ssh-copy-id ${Destino}"
conSer="ssh ${Destino}"
Back="rsync -av ${pendrive_path} ${Destino}:/home/johan/BackUp"
Subli="${pendrive_path}/Programas/Uso/sublime-text_build-3126_amd64.deb"

#Teclado Español
setxkbmap es;

#Generación de claves
${KG};
${KC};

#node & npm
sudo rsync -av ${Destino}:/usr/local/lib/ /usr/local/lib;
sudo rsync -av ${Destino}:/usr/local/bin/ /usr/local/bin;

#otros programas
	#mysql-workbench
sudo apt-get install mysql-workbench; 
	#sublime
sudo dpkg -i ${Subli};
	#chromium
sudo apt-get install chromium;
	#curl
sudo apt-get install curl;
	#git
sudo apt-get install git;

#desinstalación de programas
for prog in firefox-esr iceweasel  gimp  gimp-data aisleriot fairymax four_in a row gnome-chess gnome-klotski gnome-mahjongg gnome-mines gnome-nibbles gnome-robots gnome-sudoku gnome-tetravex hitori hoichess iagno lightsoff quadrapassel swell-foop tali xboard evolution exim4-base openjdk-7-jre apache2-bin;
do
sudo apt-get remove --purge ${prog} -y;
done;
sudo apt-get clean && sudo apt-get autoclean && sudo apt-get autoremove -y;

#Copia de Seguridad
${Back};
echo '*/10 * * * * ${Back}' > /media/user/Loki/.temp/user
sudo mv /media/user/Loki/.temp/user /var/spool/cron/crontabs/;

#Selección de idioma
sudo dpkg-reconfigure locales;

#comprovaciones
node -v;
npm -v;
sublime_text -v;

#fin de init
exit
