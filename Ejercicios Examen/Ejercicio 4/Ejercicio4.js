
/*Script paa el ejercicio 4 del módulo*/
/*Version:1.0*/
/*Autor:Fernando Johanson*/

/*Módulos requeridos*/
const os = require('os');
//const fs = require('fs');
const exec = require('child_process').exec;

/*variables*/

Servidor="192.168.100.76"
Usuario="johan"
Destino="${Usuario}@${Servidor}"
KG="ssh-keygen;"
KC="ssh-copy-id ${Destino};"
conSer="ssh ${Destino}"
Back="rsync -av /media/user/Loki ${Destino}/home/johan/prueba; "

/*funciones*/
function backUp(){
		//resinttalación de openssh-server
			exec(KG, (err,data) => {
			if (err){
				console.log('No ha instalado openssh-server')
			}
			else console.log('OK')
		   });
		//creación de clave para el servidor
		exec(KG, (err,data) => {
			if (err){
				console.log('No ha ejecutado Keygen')
			}
			else console.log('OK')
		   });
		//envio de clave al servidor
		exec(KC, (err,data) => {
			if (err){
				console.log('No ha ejecutado copy')
			}
			else console.log('OK')
		   });
		//ejecución de la copia de seguridad
		exec(Back, (err,data) => {
			if (err){
				console.log('No ha ejecutado rsync')
			}
			else console.log('OK')
		   });		
	};
backUp();
