#programa de monitorización de equipo en BASH
#v:1.0
#Fernando Johanson
#07/Abril/2017

#Permisos de SuperUsuario
sudo su;

#Instalación necesaria
apt-get install nmap;  

#Crear archivo de informe
mkdir /home/informe/
#Permisos para el nuevo directorio
chmod 777 /home/informe/ -R 
#Generación del informe
echo nombre de maquina > /home/informe/informe.txt;
hostname >> /home/informe/informe.txt;
echo __________________________________________________________________________________ >> /home/informe/informe.txt;
echo nombre de usuario >> /home/informe/informe.txt;
whoami >> /home/informe/informe.txt;
echo __________________________________________________________________________________ >> /home/informe/informe.txt;
echo fecha >> /home/informe/informe.txt;
date >> /home/informe/informe.txt;
echo __________________________________________________________________________________ >> /home/informe/informe.txt;
echo usuarios >> /home/informe/informe.txt;
who >> /home/informe/informe.txt;
echo __________________________________________________________________________________ >> /home/informe/informe.txt;
echo estado de discos; >> /home/informe/informe.txt;
df >> /home/informe/informe.txt;
echo __________________________________________________________________________________ >> /home/informe/informe.txt;
echo estado de memoria >> /home/informe/informe.txt;
free >> /home/informe/informe.txt;
echo __________________________________________________________________________________ >> /home/informe/informe.txt;
echo configuracion de red >> /home/informe/informe.txt;
sudo ifconfig >> /home/informe/informe.txt;
echo __________________________________________________________________________________ >> /home/informe/informe.txt;
sudo route -n >> /home/informe/informe.txt;
echo __________________________________________________________________________________ >> /home/informe/informe.txt;
nmap localhost >> /home/informe/informe.txt;

# echo $informe > /home/informe/informe.txt
#Salida
exit;
end;


