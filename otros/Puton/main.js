//declaraciónes
var revisio =""
//el primer objeto nos dará el listado de las bases de datos
var obj1 = {
	type 		: "get",
	url			:"",
	error: function(errMsg){ alert("algo ha fallado") },
	dataType	: "json",
	contentType	: "application/json; charset=utf-8"
	}
//el segundo objeto nos dará el listado de documentos de la BD selecionada
var obj2 = {
	type 		: "get",
	url			: "", 
	failure: function(errMsg){ console.log(errMsg) },
	dataType	: "json",
	contentType	: "application/json; charset=utf-8"
	}
//el tercer objeto nos da los contenidos del documento seleccionado
var obj3 = {
	type 		: "get",
	url			: "",
	failure: function(errMsg){ console.log(errMsg) },
	dataType	: "json",
	contentType	: "application/json; charset=utf-8"
	}
//el cuarto objeto deberá permitir modificar el documento
var obj4 = {
	type 		: "post",
	url			: "",
	dataType	: "json",
	contentType	: "application/json; charset=utf-8"
	}
//el quinto objeto está pensado para borrar el documento de la BD
var obj5 = {
	type 		: "delete",
	url			: "",
	error: function(errMsg){ alert("algo ha fallado, vuelve al código") },
	dataType	: "json",
	contentType	: "application/json; charset=utf-8"
}

//funciones
//arranque de página
$(document).ready(function(){
	$('#SELECTOR').hide()
	$('#EDITOR').hide()
	$('#brr').hide()
	$('#edit').hide()
	$('#atras').hide()
	$('#Sel2').hide()
	$('#tabla2').hide()
	$('#PUERTO').hide()
	$('#reset').click(function(){
		location.reload();
	})
    $('#atras').click(function() {
    	var s =	$('#Sel2').css('display');
   		if ( s = "none") {	    	
    		$('div').hide()
			$('#MEIN').fadeIn(1000)	
	    }
	    if 
	     ( s = "inline-block" ){
	     	$('#atras').click(function() {
	     		$('#tabla2').html("");
    			$('#Sel2').hide();
	     	})
    		
    	}	
    });   
});
//conectar con el servidor de BD
/*function conect (obj1){
	servi = $('#direcc').val() + $('#PUERTO').val()
}*/
function dire(){
	var servi = $('#direcc').val() + $('#PUERTO').val();
	ip=$('#direcc').val()
	proto=ip.substring(0,4)
	proto2="http"
	$('#entrar').click(function(){
		var proto = ip.substring(0,4)
		if (proto != proto2){
			obj1.url ="http://"+ servi + "_all_dbs";
			general();
		}else{
		obj1.url = servi + "_all_dbs"
		general();
		}
	})
	$('#Sel1').change(function(){
		var proto = ip.substring(0,4)
		if (proto != proto2){
			obj2.url ="http://"+ servi + $('#Sel1 option:selected').val() + "/_all_docs?include_docs=true";
			BDsel();
		}else{
		obj2.url = servi + $('#Sel1 option:selected').val() + "/_all_docs?include_docs=true";
		BDsel();	
		}
	})
	$('#Sel2').change(function(){
		var proto = ip.substring(0,4)
		if (proto != proto2){
			obj3.url ="http://"+ servi + $('#Sel1 option:selected').val() + "/" + $('#Sel2 option:selected').val();
			pinta();
		}else{
		obj3.url = servi + $('#Sel1 option:selected').val() + "/" + $('#Sel2 option:selected').val();
		pinta();
		}
	})
	$('#brr').click(function(){
		var proto = ip.substring(0,4)
		if (proto != proto2){
			obj5.url ="http://"+ servi + $('#Sel1 option:selected').val() + "/" + $('#Sel2 option:selected').val() + "?rev=" + $('#_rev_td')[0].innerText;
			pinta();
		}else{
		obj5.url = servi + $('#Sel1 option:selected').val() + "/" + $('#Sel2 option:selected').val() + "?rev=" + $('#_rev_td')[0].innerText;
		borrar();
		}
	})
}
//Selector de bases de datos
function general(){
	//reajuste de objeto
//var servi = $('#direcc' ).val()/* + $('#PUERTO').val()*/
	//acciones
	$('#MEIN').hide()
	$('#SELECTOR').fadeIn(1000)
	$('#atras').fadeIn(1000)
	$.ajax(obj1).done( function(data){
		$.each(data,function(idx,bd){
		//console.log(bd);
		let html = `
			<option value="${bd}">
			${bd}
			</option>
			`
			$('#Sel1').append(html);
		})
	})
}
//Selector de documento
function BDsel(){	
	//acciones
	$('#Sel2').hide()
	$('#Sel2').fadeIn(1000)
	$.ajax(obj2).done( function (data){
		$('#Sel2').html("");
		$.each(data.rows, function(idx,rows){
			//console.log(rows.id)
			let html =`
			<option value="${rows.id}">
			${rows.id}
			</option>
			`		
			$('#Sel2').append(html);
		})
	})
}
//Tabla con los datos del documento seleccionado
function pinta(){ 
	//acciones
	$.ajax(obj3).done(function(data){
		//console.log(data);
		//opciones de apariencia
		$('#tabla2').fadeIn(1000)
		$('#brr').fadeIn(1000)
		$('#edit').fadeIn(1000)
		$('#tabla2').html("")
		//encabezado
		$('#tabla2').append('<tr>')
		for(var elemen in data){
			// console.log(elemen);
			$('#tabla2').append(`<th id="${elemen + "_th"}">${elemen}</th>`);			
		};
		$('#tabla2').append('</tr>')
		//datos
		$('#tabla2').append('<tr>')
		for(var elemen in data){
		  $('#tabla2').append(`<td id="${elemen + "_td"}"> ${data[elemen]} </td>`) 		  
		}
		$('#tabla2').append('</tr>')	
	})
}
function modificar(){
	//acciones
	$.ajax(obj3).done(function(data){
		//opciones de apariencia
		$('#EDITOR').fadeIn(1000)
		//Formulario de edición
		for(var elemen in data){

			if (elemen=="_id")
				{$('#_id').hide();
		} else
			if (elemen=="_rev")
				{$('#_rev').hide();
		}	else
			{$('#MODF').append(`${elemen} <input type="text" id="${elemen}" value="${data[elemen]}"><br>`)}
		}

	})
/*	//ajuste de objeto
	obj4.url = servi + $('#Sel1 option:selected').val() + "/" + $('#Sel2 option:selected').val()
	//acciones
	function SendMod(obj4){
		JSON.stringify(	
			$.each(	$('#MODF').val(), function(){
				obj4.data=elemen + ":" + ${data[elemen]}
			})	
		)

	}
*/
}
//borrado del documento seleccionado
function borrar(obj5){
	if (confirm("¿Seguro que quieres borrar el documento selecionado?") == true){
		$.ajax(obj5).done(function(obj5){
			console.log(obj5);
			BDsel();
			$('#tabla2').html("");	
		});
	}
}


/*
function ActualizaDBs(){
	$.ajax({
	  type: "get",
	  url: servi+"_all_dbs",
	  data: null,
	  success: function(data){ console.log(data) },
	  failure: function(errMsg){ console.log(errMsg) },
	  dataType: 'json',
	  contentType: "application/json; charset=utf-8"
	});	
}
function readDB(){
	$.ajax({
	  type: "get",
	  url: servi + $('#lector').val() + "/_all_docs?include_docs=true",
	  data: null,
	  success: function(data){ console.log(data) },
	  failure: function(errMsg){ console.log(errMsg) },
	  dataType: 'json',
	  contentType: "application/json; charset=utf-8"
	});
}
*/
/*
function muestra(data){
	$.each(data, rows, function(indice, valor){
		$('#lista').append('<li>' + valor.doc.id + '</li>')
	});
}*/
