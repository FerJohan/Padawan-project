sudo 
do
# variables generales
etcDefaultLocale='/etc/default/locale'
archivoLenguaje='/etc/locale.gen'
archivoLocalTime='/etc/localtime'
lenguaje='es_ES.UTF-8 UTF-8'
kbmap='es'
bashrc='/home/user/.bashrc'
pendrive='/media/user/Jano/init'
tz='/usr/share/zoneinfo/Europe/Madrid localtime'
autostart='/home/user/.config/autostart'
# cambia los passwords de root y user
echo live > live
echo live >> live
passwd < live
passwd user < live
# establece zona horaria para cambiar Gnome y ntpdate
chmod 777 $archivoLenguaje $archivoLocalTime
cp $tz $archivoLocalTime
# lenguaje para Gnome
echo $lenguaje > $archivoLenguaje
cp $pendrive/locale.default $etcDefaultLocale
locale-gen
localectl set-keymap $kbmap
localectl set-x11-keymap $kbmap
# teclado español
echo 'setxkbmap es' >> $bashrc
mkdir $autostart
cp $pendrive/gnome-terminal.desktop $autostart/.
# instalacion de chrome
cd /media/user/Jano/Programas/Uso/Chrome
./google-chrome-stable_current_amd64.deb
PATH=$PATH:/media/user/Jano/Programas/Uso/Chrome/
./google-chrome-stable_current_amd64.deb
# preparar fondos de debian
cp /media/user/Jano/Imagenes/tux-linux-jedi-star-wars.jpg /home/user/Pictures
cp /media/user/Jano/Imagenes/338114.jpg /home/user/Pictures
# abrir settings
gnome-control-center
#terminar
done
#cerrar sesion root
exit
#cerrar terminal
exit