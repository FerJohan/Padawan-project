// funcion para volver al incio
function atr(){
	if (confirm("Seguro que quieres volver a la pantalla de inicio?\nLos cambios que no se hayan guardado se perderán.")==true){
		location.reload();
	}
}

//Creador de exàmenes
function nexam(obj1){
	$('#MEIN').hide()
	$('#EditEXAM').hide()
	$('#SETUP').fadeIn(500)
	console.log(obj1)
	
}
//la primera parte configura la plantilla para poder redactar las preguntas
function confiTest(){
	var obj1 = {
		numPreg : $('#Npreg').val(),
		numResp : $('#Nres').val(),
		dataType	: "json",
		contentType	: "application/json; charset=utf-8"
	}
	$('#parametrosEXAM').hide()
	$('#EditEXAM').fadeIn(500)
	$('#GuarPlantEx').fadeIn(500)
	var tope1 = obj1.numPreg
	var tope2 =	obj1.numResp 
	var bravo = 0;
//estos buqlues se encargará de darnos tantas preguntas y respuestas como se ha configurado
		for (alfa = 0 ; alfa < tope1; alfa++){
		$('#EditQuest').append(`<fieldset id="PREG${alfa +1}" class="pregun">
		<legend>PREGUNTA ${alfa + 1}</legend>
		<select id="X${alfa + 1}" class="Xcorrect">Seleciona la respuesta correcta</select>
		<input id="P-${alfa +1}" type="text" class="pregunta" placeholder="escriba la pregunta" style="width: 400px"><br>
		</fieldset>`);
	}
	
			while ( bravo < tope2 ) {
				$("fieldset.pregun").append(`<p>${bravo + 1}</p><input id="R-${bravo + 1}" type="text" class="respuesta" placeholder="opción de respuesta"><br>`);
				$("fieldset.pregun select.Xcorrect").append(`<option value="${bravo + 1}">${bravo + 1}</option>`)
				bravo++
			}
		bravo = 0;
	console.log(obj1)
}
//esta función se encarga de recoger la información de todo el examen para guardarlo en un documento JSON que luego será usado
function guardatest(obj2){
	//var PN=$("[id|=P]").length
	//var RN=$("[id|=R]").length
	//var charlie=PN+RN
	var charlie=$(":text.pregunta").length
	var ecco=[]
	var foxtrot=$('#EditQuest :selected').length
	var golf=[]
	var juliet=$(":text.respuesta").length
	var kilo=[]
	obj2={
		_id				: $('#materia').val() + "_" + $('#IdEx').val(),
		materia			: $('#materia').val(),
		nombre			: $('#IdEx').val(),
		numPreg			: $(".pregunta").length,
		numResp			: $(".respuesta").length,
		penalizacion	:{
			ResErrA	: $('#ResErrA').val(),
			ResErrB : $('#ResErrB').val(),
			ResErrC : $('#ResErrC').val(),
		},
		cuestionario 	: {},
		opciones		: {}, 
		respuestas		: [], 
		dataType		: "json",
		contentType		: "application/json; charset=utf-8"
	}
	for (var i=0; i<charlie;i++){	var delta=$(":text.pregunta")[i];
		ecco[i]=delta.value;
		};
		obj2.cuestionario=ecco;
	for (var i=0; i<juliet;i++){	var lima=$(':text.respuesta')[i];
		kilo[i]=lima.value;
		};
		obj2.opciones=kilo;
	for (var i=0; i<foxtrot;i++){	var hotel=$('#EditQuest :selected')[i];
		golf[i]=hotel.value;
		};
		obj2.respuestas=golf;

		firebase.database().ref('examenes/').push(obj2)
		/*var examenes = firebase.database().list('examenes')
		examenes.push(obj2)*/

		console.log(obj2)
		console.log(JSON.stringify(obj2))
	
}

//Selector de examenes en BD
function SelectExam1(){
	$('#EligEx').fadeIn(500),
	$('#MeinSel1').html("");
	$('#MeinSel1').append(`<option value="null">Materia</option>`);
	$('#MeinSel2').html("");
	firebase.database().ref('examenes').orderByChild("materia").on("child_added", function(snapshot) {	
  		//console.log(snapshot.val().nombre);
  		let html = `
			<option value="${snapshot.val().materia}">
			${snapshot.val().materia}
			</option>
			`
		$('#MeinSel1').append(html);
		$('#infoTest').html("")
	})
}
function SelectExam2(){	
	$('#MeinSel2').html("");
	$('#MeinSel2').append(`<option value="null">Nombre</option>`);	
	firebase.database().ref('examenes').orderByChild("materia").equalTo($('#MeinSel1 option:selected').val()).on("child_added", function(snapshot) {
  		console.log(snapshot.val().nombre);
  		let html = `
			<option value="${snapshot.val().nombre}">
			${snapshot.val().nombre}
			</option>
			`
		$('#MeinSel2').append(html);
	})
	$('#MeinSel2').change(function(){
		$('#infoTest').html("")
		firebase.database().ref('examenes').orderByChild("_id").equalTo( ( $('#MeinSel1 option:selected').val()+"_"+$('#MeinSel2 option:selected').val() ) ).on("child_added", function(snapshot){
			var RpP=snapshot.val().numResp/snapshot.val().numPreg
			$('#infoTest').html("")
			$('#EligEx').append(`<div id="infoTest" class="info">
				<span class="param">
				Preguntas ${snapshot.val().numPreg} <br>
				Respuestas por pregunta ${RpP} <br>
				</span>
				</div>`)
		})	
	})
}
//EXAMEN!!!! Por fin!!!
function hexam(){
	if($('#MeinSel1 option:selected').val() != "null" && $('#MeinSel2 option:selected').val() != "null"){
		$('#MEIN').hide();
		$('#EditEXAM').hide();
		$('#EXAMEN').fadeIn(500);
		firebase.database().ref('examenes').orderByChild("_id").equalTo( ( $('#MeinSel1 option:selected').val()+"_"+$('#MeinSel2 option:selected').val() ) ).on("child_added", function(snapshot){
			var mike=snapshot.val().opciones
			var november=snapshot.val().opciones.length
			var oscar=snapshot.val().cuestionario.length
			var papa=snapshot.val().cuestionario
			var quebec=november+oscar
			var romeo=november/oscar
			var i=0;
			var e=0;
			var l=romeo

			while (e<november){
				l+=e
				var sierra = mike.slice(e,l)
				$('#PRUEBA').append(`
					<fieldset class="PeX" id="p${i+1}">
					<legend>Pregunta ${i+1}</legend>
					<span class="enunciado"><b>${papa[i]}</b></span><br>
				`)
				//console.log(papa[i])
				i++
				e+=l
				for(var s=0; s<sierra.length; s++){
					$('#PRUEBA').append(`
					<input type="checkbox" class="ReX" name="${s+1}" value="${s+1}">${sierra[s]}<br>
				`)
				$("#PRUEBA").append(`	</fieldset> `)
					//console.log(sierra[s])
				}
			}

		})
	}
	else {
			alert("Esegurate de seleccionar los campos \n Materia y Nombre \n para selecionar el examen");
			SelectExam1();
		}
}

//funcion correctora
function corrige(){
	firebase.database().ref('examenes').orderByChild("_id").equalTo( ( $('#MeinSel1 option:selected').val()+"_"+$('#MeinSel2 option:selected').val() ) ).on("child_added", function(snapshot){
	var tango=snapshot.val().penalizacion
	var uniform=snapshot.val().respuestas
	var victor=[]
	var sancion=0
	var totalB=0
	var totalF=0
		for (i=0; i<$(":checked.ReX").length; i++){
			victor[i]=$(":checked.ReX")[i].name
		}
		console.log(victor)

		if (tango.ResErrA != "null"){
			victor.each(()=>{
				if(victor.value == ""){totalF += 0;}
				else if (victor.value == uniform.value){totalF += 1;}
			})
		//else {}
		}
	})
}


//aranque principal del DOM
$(document).ready(function(){
	$('#SETUP').hide()
	$('#EligEx').hide()
	$('#EXAMEN').hide()
	$('#NOTA').hide()
		if (typeof window.FileReader!=="undefined")
	{
	console.log("Soportado el File API");
	}
	else
	{
	alert("No es soportado el File API");
	}
})



//apuntes de intentos fallidos

		//console.log(mike)
		//console.log(november)
		//console.log(oscar)
		//console.log(papa)

/*		for (var i=0; i<oscar; i++){
			$('#PRUEBA').append(`
				<fieldset class="PeX" id="p${i}">
				<legend>Pregunta ${i}</legend>
				<span class="enunciado"><b>${papa[i]}</b></span><br>
				</fieldset>
			`)
			//var sierra = mike.slice(i,romeo)
			//console.log(sierra)
			//console.log(papa[i])

		}*/



/*		for (var i=0; i<oscar;i++){
			for (var e=0; e<romeo;e++){
				var sierra=i+e;
				$("fieldset.PeX").append(`
					<input type="checkbox" class="ReX" id="r${e} value="${e}">${mike[sierra]}<br>
				`)
				//console.log(mike[i+e])
			}
		}*/
		



/*		for (var i=0; i<kilo; i++){
			$('#PRUEBA').append(`
				<div id="P${i}" class="PreguntaEX">
				<label>${lima[i]}</label><br>
			`);
			console.log(lima[i]);
			i+=india;
			$('#PRUEBA').append(`</div>`)
		};
			var e=1;
			while (e<=india){
				$("div.PreguntaEX").append(`
				<input type="checkbox" name="R${i+e}" value="${e}">${lima[i+e]}<br>
				`);
				console.log(lima[i+e]);
				e++;
			};*/

	/*var file = File.createFromFileName("./Pruebas");
	var dsFile = Components.classes["@mozilla.org/file/directory_service;1"]
                    .getService(Components.interfaces.nsIProperties)
                    .get("ProfD", Components.interfaces.nsIFile);

	dsFile.append("${obj2.id}.txt");

var file = File.createFromNsIFile(dsFile);*/
		
 

/*	$.each($(':text'),function(idx,data){
		return data.value;
	})*/

/*	for (var i = 0; i < charlie -1 ; i++) {
		console.log( $('#EditQuest')[0].value )
		i++
	};*/
	//console.log($('#EditQuest')[0][0].value)
	//console.log($('#EditQuest')[0][1].value)
/*	for (var charlie = 1; charlie<PN  ; charlie++){
		var preguntas=$("[id|=P]nth-child(${charlie})").val()
		console.log(preguntas)
	}*/
 /*	$('#EditQuest input').each(function(){
		var respuestas={
			P_id		: $("pregun").id,
			enunciado 	: $("pregunta").val(),
			respuestas 	: $('[id|=R]').val()
		};
	});
	$(".pregunta, .respuesta").val()*/